db.fruits.countDocuments()

db.fruits.aggregate([
    {
        $match: {count: {$gt:20}}
    },
    {
        averagePrice: {$avg: "$price"}
    },
    {
        highestPrice: {$max: "$quantity"}
    },
    {
        lowestPrice: {$min: "$quantity"}
    }
])